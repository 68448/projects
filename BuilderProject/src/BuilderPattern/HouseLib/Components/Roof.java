package BuilderPattern.HouseLib.Components;

public class Roof {
    private final String type;

    public Roof(String type){
        this.type = type;
    }
}
