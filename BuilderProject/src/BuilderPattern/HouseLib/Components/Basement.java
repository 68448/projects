package BuilderPattern.HouseLib.Components;

public class Basement {
    private final int depth;
    private final int width;
    private final int length;
    private final String type;

    public Basement(int depth, int width, int length, String type){
        this.depth = depth;
        this.width = width;
        this.length = length;
        this.type = type;

    }
}
