package BuilderPattern.HouseLib.Components;

public class Windows {
    private final float height;
    private final float width;

    public Windows(float height, float width){
        this.height = height;
        this.width = width;
    }
}
