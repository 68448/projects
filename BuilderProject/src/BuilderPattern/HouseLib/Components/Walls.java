package BuilderPattern.HouseLib.Components;

public class Walls {
    private final float height;
    private final float thickness;

    public Walls(float height, float thickness){
        this.height = height;
        this.thickness = thickness;
    }
}
