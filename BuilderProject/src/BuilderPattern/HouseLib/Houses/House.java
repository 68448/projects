package BuilderPattern.HouseLib.Houses;

import BuilderPattern.HouseLib.Components.Basement;
import BuilderPattern.HouseLib.Components.Roof;
import BuilderPattern.HouseLib.Components.Walls;
import BuilderPattern.HouseLib.Components.Windows;

public class House {
    private Basement basement;
    private boolean foundation;
    private boolean carcass;
    private Walls walls;
    private Windows windows;
    private boolean ceiling;
    private Roof roof;

    public House(Basement basement, boolean foundation, boolean carcass, Walls walls, Windows windows, boolean ceiling, Roof roof){
        this.basement = basement;
        this.foundation = foundation;
        this.carcass = carcass;
        this.walls = walls;
        this.windows = windows;
        this.ceiling = ceiling;
        this.roof = roof;
    }
    public void message(){
        System.out.println("House has been finished");
    }
}
