package BuilderPattern.Builders;

import BuilderPattern.HouseLib.Components.Basement;
import BuilderPattern.HouseLib.Components.Roof;
import BuilderPattern.HouseLib.Components.Walls;
import BuilderPattern.HouseLib.Components.Windows;

public interface Builder {
    void setBasement(Basement basement);
    void setFoundation(boolean foundation);
    void setCarcass(boolean carcass);
    void setWalls(Walls walls);
    void setWindows(Windows windows);
    void setCeiling(boolean ceiling);
    void setRoof(Roof roof);
}
