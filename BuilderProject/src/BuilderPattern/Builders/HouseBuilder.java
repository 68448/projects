package BuilderPattern.Builders;

import BuilderPattern.HouseLib.Components.Basement;
import BuilderPattern.HouseLib.Components.Roof;
import BuilderPattern.HouseLib.Components.Walls;
import BuilderPattern.HouseLib.Components.Windows;
import BuilderPattern.HouseLib.Houses.House;

public class HouseBuilder implements Builder {

    private Basement basement;
    private boolean foundation;
    private boolean carcass;
    private Walls walls;
    private Windows windows;
    private boolean ceiling;
    private Roof roof;



    @Override
    public void setBasement(Basement basement) {
        this.basement = basement;
    }

    @Override
    public void setFoundation(boolean foundation) {
        this.foundation = foundation;
    }

    @Override
    public void setCarcass(boolean carcass) {
        this.carcass = carcass;
    }

    @Override
    public void setWalls(Walls walls) {
        this.walls = walls;
    }

    @Override
    public void setWindows(Windows windows) {
        this.windows = windows;
    }

    @Override
    public void setCeiling(boolean ceiling) {
        this.ceiling = ceiling;
    }

    @Override
    public void setRoof(Roof roof) {
        this.roof = roof;
    }

    public House result(){
        return new House(basement, foundation, carcass, walls, windows, ceiling, roof);
    }
}
