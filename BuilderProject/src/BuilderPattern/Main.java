package BuilderPattern;

import BuilderPattern.Builders.Builder;
import BuilderPattern.Builders.HouseBuilder;
import BuilderPattern.Directors.Director;
import BuilderPattern.HouseLib.Houses.House;

public class Main {

    public static void main(String[] args){
        Director director = new Director();
        HouseBuilder houseBuilder = new HouseBuilder();
        director.constructHouse(houseBuilder);

        House house = houseBuilder.result();
        house.message();
    }
}
