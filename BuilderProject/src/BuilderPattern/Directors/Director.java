package BuilderPattern.Directors;

import BuilderPattern.Builders.Builder;
import BuilderPattern.HouseLib.Components.Basement;
import BuilderPattern.HouseLib.Components.Roof;
import BuilderPattern.HouseLib.Components.Walls;
import BuilderPattern.HouseLib.Components.Windows;

public class Director {
    public void constructHouse(Builder builder){
        builder.setBasement(new Basement(5, 70, 120, "Concrete"));
        builder.setFoundation(true);
        builder.setCarcass(true);
        builder.setWalls(new Walls(2.8F, 3.0F));
        builder.setWindows(new Windows(1.5F,0.5F));
        builder.setCeiling(true);
        builder.setRoof(new Roof("Gable"));
    }


}
