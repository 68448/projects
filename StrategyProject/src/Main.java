import java.util.Scanner;
public class Main {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        String action;
        action = in.nextLine();
        switch (action){
            case "Add":
                Strategy strategyAdd = new StrategyAdd();
                System.out.println(strategyAdd.execute(10, 5));
                break;
            case "Substract":
                Strategy strategySub = new StrategySubstract();
                System.out.println(strategySub.execute(10,5));
                break;
            case "Multiply":
                Strategy strategyMult = new StrategyMultiply();
                System.out.println(strategyMult.execute(10, 5));
                break;
            case "Division":
                Strategy strategyDiv = new StrategyDivision();
                System.out.println(strategyDiv.execute(10,5));
                break;
            default:
                System.out.println("Wrong Command");
                break;
        }
    }
}
