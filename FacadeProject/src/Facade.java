public class Facade {
    int var;

    public Facade(int var){
        this.var = var;
    }

    public void facadeMethod(){
        if(this.var == 1){
            Foo fu = new Foo();
            fu.foo();
        }
        if(this.var == 2){
                Bar br = new Bar();
                br.bar();
        }else {
            if (this.var == 3){
                Bar br = new Bar();
                Foo fu = new Foo();

                fu.foo();
                br.bar();
            }

        }
    }
}
