public interface AbstractFactory {
    Sedan createSedan();
    Coupe createCoupe();
}
interface Sedan{}
interface Coupe{}