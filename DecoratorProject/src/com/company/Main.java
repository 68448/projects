package com.company;

public class Main {

    public static void main(String[] args) {
        String string = "fubar!";
        Component uppercaseComponent = new CertainDecorator(new CertainComponent());
        uppercaseComponent.foo(string);
        //Component component  = new CertainComponent();
        //component.foo(string);
    }
}
