package com.company;

public class ParentalDecorator implements Component {
    private Component wrappee;

    ParentalDecorator(Component component){
        this.wrappee = component;
    }

    @Override
    public void foo(String string) {
        wrappee.foo(string);
    }
}
