package com.company;

public class CertainDecorator implements Component {
    Component component;
    CertainDecorator(Component component) {
        this.component = component;
    }

    @Override
    public void foo(String string) {
        component.foo(wrappingMethod(string));
    }

    private String wrappingMethod(String string){
        return string.toUpperCase();
    }
}
